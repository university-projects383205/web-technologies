<?php
  session_start();

  if (isset($_SESSION["username"])) {
?>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <link href="img/rss.png" type="image/png" rel="shortcut icon"/>
        <link href="css/accordion.css" type="text/css" rel="stylesheet"/>
        <link href="css/dashboard.css" type="text/css" rel="stylesheet"/>
        <link href="css/global.css" type="text/css" rel="stylesheet"/>
        <link href="css/index.css" type="text/css" rel="stylesheet"/>
        <link href="css/navbar.css" type="text/css" rel="stylesheet"/>
        <link href="css/news.css" type="text/css" rel="stylesheet"/>
        <link href="css/pages.css" type="text/css" rel="stylesheet"/>
        <link href="css/search.css" type="text/css" rel="stylesheet"/>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="js/index.js"></script>
        <title>Feed Stacker</title>
      </head>
      <body>
        <div id="frame">
          <div id="sidebar">
            <nav>
              <div id="account">
                <img src="img/account_white.png" alt="Account icon"/>
                <p>Welcome back,<br/><strong id="display_name"><?= $_SESSION["username"] ?></strong>!</p>
              </div>
              <hr/>
              <ul>
                <li id="dashboard" class="operations" data-is-selected="true"><img src="img/gauge_white.png"/>Dashboard</li>
                <li id="news" class="operations" data-is-selected="false"><img src="img/news_white.png"/>News</li>
                <li id="logout" class="operations" data-is-selected="false"><img src="img/logout_white.png"/>Logout</li>
              </ul>
            </nav>
          </div>
          <div id="view"></div>
        </div>
      </body>
    </html>
<?php
  }
  else {
    $_SESSION["msg"] = "You have to login first!";
    header("Location: login.html");
  }
?>
