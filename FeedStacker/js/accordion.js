// Enable accordion animations
$(document).ready(function() {
  // Let only one tab opened at a time
  $("div#accordion div.content").hide();
  $("div#accordion h2#add").next().show();

  $("div#accordion h2").click(function() {
    if ($(this).next().css("display") === "none") {
      $("div#accordion div.content").slideUp();
      $(this).next().slideDown();
    }
  });
});
