window.pages = new Array();
window.elements_per_page = 18;

$(document).ready(function() {
  //Enable feeds flip-card style
  function enableFlip() {
    $("img#flip").click(function() {
      $(this).parent().parent().css("transform", "rotateY(180deg)");
    });

    $("div.back").mouseleave(function() {
      $(this).parent().css("transform", "rotateY(0deg)");
    });
  }

  // Create flip-card for feeds
  function update_pattern(json_obj) {
    var elem_pattern = "\
      <div class='news'>\
        <div class='front'>\
          <img id='flip' src='img/flip.png' alt='Flip'/>\
          <img id='icon' src='$icon' alt='Icon'/>\
          <br/>\
          <a target='_blank' href='$link'>$title</a>\
        </div>\
        <div class='back'>\
          <p>$description</p>\
        </div>\
      </div>\
    ";

    elem_pattern = elem_pattern.replace("$icon", json_obj.icon);
    elem_pattern = elem_pattern.replace("$link", json_obj.link);
    elem_pattern = elem_pattern.replace("$title", json_obj.title);
    elem_pattern = elem_pattern.replace("$description", json_obj.description);

    return elem_pattern;
  }

  // Split feeds in pages
  function setupPages(total_news) {
    const pages = Math.ceil(total_news / window.elements_per_page);
    var to_display = "";

    if (pages > 0) {
      var i = 0;
      to_display = "<div id='pages'>";
      to_display += "<img id='prev' src='img/previous.png' alt='Previous'/>";
      to_display += "<img id='page1' class='page' src='img/small-circle_selected.png' alt='1'/>";

      for (i = 1; i < pages; i++) {
        to_display += "<img id='page" + (i + 1) + "' class='page' src='img/small-circle_unselected.png' alt='" + (i + 1) + "'/>";
      }

      to_display += "<img id='next' src='img/next.png' alt='Next'/>";
      to_display += "</div>"
    }

    return to_display;
  }

  // Split news in various parts
  function splitNews(news) {
    var splitted_news = new Array();
    var news_fragment = new Array();
    var j = 0;

    for (var i = 0; i < news.length; i++) {
      news_fragment.push(news[i]);
      j += 1;

      if (j == window.elements_per_page) {
        splitted_news.push(news_fragment);
        news_fragment = [];
        j = 0;
      }
    }

    if (j != 0) {
      splitted_news.push(news_fragment);
    }

    return splitted_news;
  }

  // Create a single page
  function createPage(index, news) {
    page = "<div id='page" + (index + 1) + "'>";

    for (var i = 0; i < news.length; i++) {
      page += update_pattern(news[i]);
    }

    page += "</div>";
    window.pages[index] = page;
  }

  // Parse the feeds which are in JSON format
  function parseNews(data) {
    const news = $.parseJSON(data);
    var pages = setupPages(news.length);
    var splitted_news = splitNews(news);

    for (var i = 0; i < splitted_news.length; i++) {
      createPage(i, splitted_news[i]);
    }

    var to_display = "<div id='result'>";
    to_display += window.pages[0];
    to_display += "</div>";

    if (pages != "") {
      to_display += pages;
    }

    return to_display;
  }

  // Enable navigator for feeds pages
  function enableNavigator() {
    // Move to previous page
    $("img#prev").click(function() {
      const curr_page_obj = $("div#pages > img[src='img/small-circle_selected.png']");
      const curr_page = "img#" + curr_page_obj.attr("id");
      const prev_page = "img#page" + (parseInt(curr_page_obj.attr("alt")) - 1);

      if (parseInt($(prev_page).attr("alt")) == 1) {
        $("img#prev").css("visibility", "hidden");
      }

      if ($("img#next").css("visibility") == "hidden") {
        $("img#next").css("visibility", "visible");
      }

      $(curr_page).prop("src", "img/small-circle_unselected.png");
      $(prev_page).prop("src", "img/small-circle_selected.png");

      $("div#result").fadeOut(function() {
        $("div#result").html(window.pages[parseInt($(prev_page).attr("alt")) - 1]);
        $("div#result").fadeIn();
        enableFlip();
      });
    });

    // Move to next page
    $("img#next").click(function() {
      const curr_page_obj = $("div#pages > img[src='img/small-circle_selected.png']");
      const curr_page = "img#" + curr_page_obj.attr("id");
      const next_page = "img#page" + (parseInt(curr_page_obj.attr("alt")) + 1);

      if (parseInt($(next_page).attr("alt")) == $("img.page").length) {
        $("img#next").css("visibility", "hidden");
      }

      if ($("img#prev").css("visibility") == "hidden") {
        $("img#prev").css("visibility", "visible");
      }

      $(curr_page).prop("src", "img/small-circle_unselected.png");
      $(next_page).prop("src", "img/small-circle_selected.png");

      $("div#result").fadeOut(function() {
        $("div#result").html(window.pages[parseInt($(next_page).attr("alt")) - 1]);
        $("div#result").fadeIn();
        enableFlip();
      });
    });

    $("img.page").click(function() {
      const curr_page_obj = $("div#pages > img[src='img/small-circle_selected.png']");
      const curr_page = "img#" + curr_page_obj.attr("id");

      if (parseInt($(this).attr("alt")) == 1) {
        $("img#prev").css("visibility", "hidden");
        $("img#next").css("visibility", "visible");
      }
      else if (parseInt($(this).attr("alt")) == $("img.page").length) {
        $("img#prev").css("visibility", "visible");
        $("img#next").css("visibility", "hidden");
      }
      else {
        $("img#prev").css("visibility", "visible");
        $("img#next").css("visibility", "visible");
      }

      $(curr_page).prop("src", "img/small-circle_unselected.png");
      $(this).prop("src", "img/small-circle_selected.png");
      const selected_page = this;

      $("div#result").fadeOut(function() {
        $("div#result").html(window.pages[parseInt($(selected_page).attr("alt")) - 1]);
        $("div#result").fadeIn();
        enableFlip();
      });
    });
  }

  // Show news
  function showNews() {
    const preloader = "\
      <div class='preloader'>\
        <img src='img/broken-circle.gif'/>\
      </div>\
    ";

    $("div#view").html(preloader);

    $.get(
      "php/main.php"
    )
    .done(function(data) {
      // Show news only if the user is still in the "News" tab
      if ($("li#news").attr("data-is-selected") === "true") {
        if (data !== "[]") {
          const to_display = parseNews(data);

          $("div#view").html(to_display);

          $("div#result").hide();
          $("div#result").fadeIn("slow", function() {
            enableFlip();
            enableNavigator();
          });
        }
        else {
          $("div#view").html("Nothing to show!");
        }
      }
    });
  }

  // Add a feed
  function addFeed(feed_url) {
    const preloader = "\
      <div id='preloader'>\
        <img src='img/progress.gif'/>\
      </div>\
    ";

    $("div#accordion div#preloader").remove();
    $("div#accordion h2#add").next().append(preloader);

    $.get(
      "php/main.php",
      {
        url: feed_url
      }
    )
    .done(function(data) {
      $("div#accordion div#preloader").remove();

      var msg = "";
      if (data.length === 0) {
        msg = "\
          <div id='preloader'>\
            <p>\
              Added <strong>" + feed_url + "</strong>!\
            </p>\
          </div>\
        ";
      }
      else {
        msg = "\
          <div id='preloader'>\
            <p>\
              Can't add <strong>" + feed_url + "</strong>!\
            </p>\
          </div>\
        ";
      }

      $("div#accordion h2#add").next().append(msg);

      $("input#url").val("");
      $("input#add").prop("disabled", true);
    })
  }

  // Show the dashboard
  function showDashboard() {
    $.get("html/dashboard.html")
    .done(function(data) {
      $("div#view").html(data);
      $.get("js/accordion.js");

      $("input#url").keyup(function() {
        if ($(this).val() === "") {
          $("input#add").prop("disabled", true);
        }
        else {
          $("input#add").prop("disabled", false);
        }
      });

      $("form#addform").submit(function() {
        event.preventDefault();
        addFeed($("input#url").val());
      });

      $("div#accordion h2#delete").click(function() {
        $.get("js/cleaner.js");
      });
    });
  }

  // Logout the current user
  function logout() {
    const preloader = "\
      <div class='preloader'>\
        <img src='img/broken-circle.gif'/>\
      </div>\
    ";

    $("div#view").html(preloader);

    $.post(
      "php/login.php",
      {
        op: "logout"
      }
    )
    .done(function(data) {
      window.location.href = "login.html";
    });
  }

  // Search between followed feeds
  function startSearch() {
    $.get("html/search.html")
    .done(function(data) {
      const preloader = "\
        <div class='preloader'>\
          <img src='img/broken-circle.gif'/>\
          <p><strong>Downloading news ...</strong></p>\
        </div>\
      ";

      $("div#view").html(data);
      $("div#view div#searchbox").after(preloader);
      $("div#searchbox > input#tofind").prop("disabled", true);

      $.get("php/main.php")
      .done(function(data) {
        $("div#view div.preloader").remove();

        if (data !== "[]") {
          const to_display = parseNews(data);
          const news = $.parseJSON(data);

          $("div#searchbox > input#tofind").prop("disabled", false);
          $("div#view div#matching").html(to_display);
          $("div#result").hide();
          $("div#result").fadeIn("slow", function() {
            enableFlip();
            enableNavigator();
          });

          var prev_val = "";
          $("div#searchbox > input#tofind").keyup(function() {
            if ($(this).val() !== prev_val) {
              var matching = new Array();

              for (var i = 0; i < news.length; i++) {
                if (news[i].title.toLowerCase().includes($(this).val().toLowerCase())) {
                  matching.push(news[i]);
                }
              }

              if ($("li#news").attr("data-is-selected") === "true") {
                const json_obj = JSON.stringify(matching);

                if (json_obj !== JSON.stringify([])) {
                  const to_display = parseNews(json_obj);

                  $("div#view div#matching").html(to_display);
                  $("div#result").hide();
                  $("div#result").fadeIn("slow", function() {
                    enableFlip();
                    enableNavigator();
                  });
                }
                else {
                  const to_display = "\
                    <div id='error'>\
                      <p>No results found for <strong>" + $(this).val() + "</strong></p>\
                    </div>\
                  ";

                  $("div#view div#matching").html(to_display);
                }
              }

              prev_val = $(this).val();
            }
          });
        }
        else {
          $("div#view div#matching").html("\
            <div class='preloader'>\
              <p><strong>Nothing to show!</strong></p>\
            </div>\
          ");
        }
      });
    });
  }

  // If image link is broken, change it with a default icon
  $("img#icon").on("error", function() {
    $(this).attr("src", "img/rss.png");
  });

  /*
  $("button#refresh").click(function() {
    showNews();
  });
  */

  // Get the display name of the current user
  $.get(
    "php/main.php",
    {
      manage: "display_name"
    }
  )
  .done(function(data) {
    const display_name = $.parseJSON(data);

    if (display_name !== "") {
      $("strong#display_name").html(display_name);
    }
  });
  $("li#dashboard").css("background-color", "rgba(0, 0, 0, 0.5)");
  showDashboard();

  // Enable navbar
  $("li.operations").click(function() {
    $("li.operations").attr("data-is-selected", "false");
    $("li.operations").css("background-color", "");
    $(this).attr("data-is-selected", "true");
    $(this).css("background-color", "rgba(0, 0, 0, 0.5)");

    if ($(this).attr("id") == "dashboard") {
      showDashboard();
    }
    else if ($(this).attr("id") == "news") {
      startSearch();
    }
    else if ($(this).attr("id") == "logout") {
      logout();
    }
  });
});
