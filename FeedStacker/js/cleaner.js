$(document).ready(function() {
  // List all the channels associated to a user
  function listFeeds(feeds) {
    var table = "<table>";

    for (var i = 0; i < feeds.length; i++) {
      table += "\
        <tr>\
          <td>\
            " + feeds[i] + "\
          </td>\
          <td class='action'>\
            <button data-row='" + i + "'>X</button>\
          </td>\
        </tr>\
      ";
    }

    table += "</table>";

    $("div#accordion h2#delete").next().html(table);
  }

  // Update the channel list
  function update() {
    $.get(
      "php/main.php",
      {
        manage: "list"
      }
    )
    .done(function(data) {
      const feeds = $.parseJSON(data);
      listFeeds(feeds);

      $("div#accordion div.content table button").click(function() {
        $.get(
          "php/main.php",
          {
            manage: "delete",
            url: feeds[$(this).attr("data-row")]
          }
        )
        .done(function(data) {
          update();
        });
      });
    });
  }

  update();
});
