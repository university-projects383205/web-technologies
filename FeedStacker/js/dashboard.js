$(document).ready(function() {
  // Add a new channel to the user list
  function addFeed(feed_url) {
    const preloader = "\
      <div id='preloader'>\
        <img src='img/progress.gif'/>\
      </div>\
    ";

    $("div#accordion div#preloader").remove();
    $("div#accordion h2#add").next().append(preloader);

    $.get(
      "php/main.php",
      {
        url: feed_url
      }
    )
    .done(function(data) {
      $("div#accordion div#preloader").remove();

      // Set "alert" messages
      var msg = "";
      if (data.length === 0) {
        msg = "\
          <div id='preloader'>\
            <p>\
              Added <strong>" + feed_url + "</strong>!\
            </p>\
          </div>\
        ";
      }
      else {
        msg = "\
          <div id='preloader'>\
            <p>\
              Can't add <strong>" + feed_url + "</strong>!\
            </p>\
          </div>\
        ";
      }

      $("div#accordion h2#add").next().append(msg);

      $("input#url").val("");
      $("input#add").prop("disabled", true);
    })
  }

  // Show the dashboard
  function showDashboard() {
    $.get("html/dashboard.html")
    .done(function(data) {
      $("div#view").html(data);
      $.get("js/accordion.js");

      $("input#url").keyup(function() {
        if ($(this).val() === "") {
          $("input#add").prop("disabled", true);
        }
        else {
          $("input#add").prop("disabled", false);
        }
      });

      $("form#addform").submit(function() {
        event.preventDefault();
        addFeed($("input#url").val());
      });

      $("div#accordion h2#delete").click(function() {
        $.get("js/cleaner.js");
      });
    });

    showDashboard();
  }
});
