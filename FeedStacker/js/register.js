$(document).ready(function() {
  $("div#back img#showpwd_reg").click(function() {
    if ($("div#back input#passwd").attr("type") === "password") {
      $("div#back input#passwd").attr("type", "text");
      $(this).attr("src", "img/eye-off.png");
    }
    else if ($("div#back input#passwd").attr("type") === "text") {
      $("div#back input#passwd").attr("type", "password");
      $(this).attr("src", "img/eye.png");
    }
  });

  $("div#back img#showpwd_check").click(function() {
    if ($("div#back input#passwdcheck").attr("type") === "password") {
      $("div#back input#passwdcheck").attr("type", "text");
      $(this).attr("src", "img/eye-off.png");
    }
    else if ($("div#back input#passwdcheck").attr("type") === "text") {
      $("div#back input#passwdcheck").attr("type", "password");
      $(this).attr("src", "img/eye.png");
    }
  });

  // Check and try to register the user
  $("div#login div#back input#register").click(function() {
    event.preventDefault();

    var params = {
      user: $("div#login div#back input#user").val(),
      passwd: $("div#login div#back input#passwd").val(),
      passwdcheck: $("div#login div#back input#passwdcheck").val()
    };

    if ($("div#login div#back input#displayname").val() !== "") {
      params.displayname = $("div#login div#back input#displayname").val();
    }

    $.post(
      "php/register.php",
      params
    )
    .done(function(data) {
      const json_obj = $.parseJSON(data);

      if (json_obj.success) {
        $("div#login div#content").css("transform", "rotateY(0deg)");
        $("div#login div#front form#login p.notify").remove();
        $("form#login input#login").after("<p class='notify'>" + json_obj.msg + "</p>");
      }
      else {
        $("div#login div#back form#register p.notify").remove();
        $("form#register input#register").after("<p class='notify'>" + json_obj.msg + "</p>");
      }

      $("div#login div#back input#user").val("");
      $("div#login div#back input#passwd").val("");
      $("div#login div#back input#passwdcheck").val("");
      $("div#login div#back input#displayname").val("");

      // alert(json_obj.msg);
    });
  });
});
