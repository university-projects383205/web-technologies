$(document).ready(function() {
  // Try to login
  $.get("php/login.php")
  .done(function(data) {
    try {
      window.location.href = $.parseJSON(data).moved;
    }
    catch (error) {
      $("img#showpwd").click(function() {
        if ($("#passwd").attr("type") === "password") {
          $("#passwd").attr("type", "text");
          $(this).attr("src", "img/eye-off.png");
        }
        else if ($("#passwd").attr("type") === "text") {
          $("#passwd").attr("type", "password");
          $(this).attr("src", "img/eye.png");
        }
      });

      // Enable various login animations
      $("div#login div#front input#login").mouseover(function() {
        $(this).attr("src", "img/login_on.png");
      });

      $("div#login div#front input#login").mouseout(function() {
        $(this).attr("src", "img/login.png");
      });

      $("div#login div#back input#register").mouseover(function() {
        $(this).attr("src", "img/register_on.png");
      });

      $("div#login div#back input#register").mouseout(function() {
        $(this).attr("src", "img/register.png");
      });

      $("div#login div#front label#register").click(function() {
        $("div#login div#content").css("transform", "rotateY(180deg)");
        $("div#login div#front form#login p.notify").remove();
      });

      $("div#login div#back label#login").click(function() {
        $("div#login div#content").css("transform", "rotateY(0deg)");
        $("div#login div#back form#register p.notify").remove();
      });
    }
  });
});
