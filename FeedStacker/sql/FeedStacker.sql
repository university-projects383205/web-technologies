-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Feb 01, 2020 alle 23:27
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `FeedStacker`
--
CREATE DATABASE IF NOT EXISTS `FeedStacker` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `FeedStacker`;

-- --------------------------------------------------------

--
-- Struttura della tabella `following`
--

CREATE TABLE IF NOT EXISTS `following` (
  `user` int(11) NOT NULL,
  `website` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feed_path` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user`,`website`,`feed_path`),
  KEY `website` (`website`,`feed_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `following`
--

INSERT INTO `following` (`user`, `website`, `feed_path`) VALUES
(1, 'thehackernews.com', 'https://feeds.feedburner.com/TheHackersNews'),
(2, 'lastampa.it', '/rss');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`ID`, `username`, `password`, `display_name`) VALUES
(1, 'daniele', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 'Daniele'),
(2, 'mario', '30cc6dd8ef8458e679e13ae3bf3f634cace9810e2eea03bb6487904595f41056', 'Mario Rossi');

-- --------------------------------------------------------

--
-- Struttura della tabella `websites`
--

CREATE TABLE IF NOT EXISTS `websites` (
  `scheme` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'http://',
  `base_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feed_path` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `same_host` tinyint(4) NOT NULL,
  `icon` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`base_url`,`feed_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `websites`
--

INSERT INTO `websites` (`scheme`, `base_url`, `feed_path`, `same_host`, `icon`) VALUES
('http://', 'lastampa.it', '/rss', 1, 'https://www.lastampastatic.it/cless/lastampa/2019-v1/img/lastampa-social.png'),
('http://', 'thehackernews.com', 'https://feeds.feedburner.com/TheHackersNews', 0, 'https://1.bp.blogspot.com/-99u3lD49_GI/W0EaiVAq7vI/AAAAAAAAxak/K_tkbQkKFq0zrPWTM0tFuvnjDrKWXnhxACLcBGAs/s100-e300/favicon.ico'),
('http://', 'xml2.corriereobjects.it', '/rss/homepage.xml', 1, NULL);

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `following`
--
ALTER TABLE `following`
  ADD CONSTRAINT `following_ibfk_1` FOREIGN KEY (`website`,`feed_path`) REFERENCES `websites` (`base_url`, `feed_path`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `following_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
