<?php
  session_start();

  include("Manager.php");

  // The major part of the "output" is encoded in JSON because it's meant to be used via AJAX requests
  if (isset($_POST["op"])) {
    if ($_POST["op"] == "logout") {
      session_unset();
      session_destroy();

      echo json_encode(["msg" => "Successfully logged out!"], JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
  }
  else if (isset($_SESSION["username"])) {
    // If the user is already logged in, redirect the client towards index (the redirect happens client-side via JavaScript)
    echo json_encode(["moved" => "index.php"], JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
  }
  else {
    if (isset($_POST["user"]) && isset($_POST["passwd"])) {
      $manager = new Manager();
      $username = $_POST["user"];
      $password = $_POST["passwd"];

      if ($manager->userExists($username, $password)) {
        $_SESSION["username"] = $username;
        header("Location: ../index.php");
      }
      else {
        echo json_encode(["msg" => "Invalid username and/or password!"], JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        $_SESSION["msg"] = "Invalid username and/or password!";
        header("Location: ../login.html");
      }
    }
    else {
      echo json_encode(["msg" => "You have to login first!"], JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

      $_SESSION["msg"] = "You have to login first!";
      header("Location: ../login.html");
    }
  }
?>
