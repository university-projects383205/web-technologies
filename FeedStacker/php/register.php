<?php
  include("Manager.php");

  if (isset($_POST["user"]) && isset($_POST["passwd"]) && isset($_POST["passwdcheck"])) {
    $manager = new Manager();
    $display_name = isset($_POST["displayname"]) ? $_POST["displayname"] : NULL;
    $username = $_POST["user"];
    $password = $_POST["passwd"];
    $password_check = $_POST["passwdcheck"];

    // Check that all the requested fields are valid
    if ($username != "" && $password != "" && $password_check != "" && $password == $password_check && $manager->newUser($username, $password, $display_name)) {
      echo json_encode(["success" => true, "msg" => "User successfully created!"], JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
    else {
      echo json_encode(["success" => false, "msg" => "Can't create user!"], JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
  }
?>
