<?php
  session_start();

  // This file controls the main features
  if (isset($_SESSION["username"])) {
    include("Reader.php");
    include("cleaner.php");

    $username = $_SESSION["username"];

    // Different parameters have different meanings
    if (isset($_GET["manage"])) {
      $manage = $_GET["manage"];

      // Lists user feeds
      if ($manage == "list") {
        getUserFeeds($username);
      }
      // Delete a feed from use's list (use the specified URL as an identifier)
      else if ($manage == "delete") {
        if (isset($_GET["url"])) {
          $to_delete = urlencode($_GET["url"]);
          deleteUserFeed($username, $to_delete);
        }
      }
      // Get the user display name
      else if ($manage == "display_name") {
        $manager = new Manager();
        $display_name = $manager->getDisplayName($username);

        echo json_encode($display_name, JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
      }
    }
    // Try to add a new channel based on the specified URL
    else if (isset($_GET["url"])) {
      $url = urlencode($_GET["url"]);

      $collector = new Collector($url);
      $added = $collector->addFeed($username);

      if (!$added) {
        echo json_encode(urldecode($url), JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
      }
    }
    // With no arguments, retrieve the news from the subscribed channels
    else {
      $reader = new Reader($username);
      $news = $reader->getNews();

      echo json_encode($news, JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
  }
  else {
    $_SESSION["msg"] = "You have to login first!";
    header("Location: ../login.html");
  }
?>
