<?php
  // This class has to communicate and query the MySQL database
  class Manager {
    private const DSN = "mysql:host=localhost;dbname=FeedStacker";
    private const USER = "root";
    private const PASSWORD = "admin";
    private $db;

    public function __construct() {
      try {
        $this->db = new PDO(Manager::DSN, Manager::USER, Manager::PASSWORD);
      }
      catch (PDOException $ex) {
        $this->db = NULL;
        echo "<strong>Can't connect to DB!</strong>";
      }
    }

    public function getPDO() {
      return $this->db;
    }

    // Get the feed link, based on the specified base URL
    public function getFeedLink($base_url) {
      // If feed_path is not specified and there is already a record with the same base_url, return the first matching record related feed_path
      $feed_url = $base_url;
      $base_url = $this->db->quote(parse_url($base_url, PHP_URL_HOST));

      $query = "
        SELECT feed_path
        FROM websites
        WHERE base_url = $base_url
        LIMIT 1;
      ";

      $rows = $this->db->query($query);

      if ($rows && $rows->rowCount() > 0) {
        if ($this->getSameHost($feed_url)) {
          return $feed_url . $rows->fetch(PDO::FETCH_ASSOC)["feed_path"];
        }
        else {
          return $rows->fetch(PDO::FETCH_ASSOC)["feed_path"];
        }
      }
      else {
        return NULL;
      }
    }

    // Remove the common prefix from the specified URL
    public function trimURLPrefix($url) {
      $host = parse_url($url, PHP_URL_HOST);

      return str_replace("www.", "", $host);
    }

    // Check if the specified URL has been already added
    public function websitePresent($base_url, $feed_path = NULL) {
      $base_url = $this->db->quote($this->trimURLPrefix($base_url));

      if ($feed_path == NULL) {
        // If feed_path is not specified, check if exist at least one record with the specified base_url
        $query = "
          SELECT base_url
          FROM websites
          WHERE base_url = $base_url
          LIMIT 1;
        ";
      }
      else {
        $feed_path = $this->db->quote($feed_path);

        $query = "
          SELECT base_url
          FROM websites
          WHERE base_url = $base_url AND feed_path = $feed_path;
        ";
      }

      $rows = $this->db->query($query);

      return ($rows && $rows->rowCount() == 1);
    }

    // Get the icon associated to the specified URL
    public function getWebsiteIcon($base_url) {
      $base_url = $this->db->quote(parse_url($base_url, PHP_URL_HOST));

      $query = "
        SELECT icon
        FROM websites
        WHERE base_url = $base_url;
      ";

      $rows = $this->db->query($query);

      if ($rows && $rows->rowCount() > 0) {
        return $rows->fetch(PDO::FETCH_ASSOC)["icon"];
      }
      else {
        return NULL;
      }
    }

    // Add a new record representing a new RSS channel
    public function addWebsite($scheme, $base_url, $feed_path, $same_host, $icon) {
      $added = false;

      $scheme = $this->db->quote($scheme);
      $base_url = $this->db->quote($base_url);
      $feed_path = $this->db->quote($feed_path);
      $same_host = intval($same_host);
      $icon = $this->db->quote($icon);

      if ($icon == "") {
        $query = "
          INSERT INTO websites(scheme, base_url, feed_path, same_host, icon)
          VALUES ($scheme, $base_url, $feed_path, $same_host, NULL);
        ";
      }
      else {
        $query = "
          INSERT INTO websites(scheme, base_url, feed_path, same_host, icon)
          VALUES ($scheme, $base_url, $feed_path, $same_host, $icon);
        ";
      }

      try {
        $this->db->beginTransaction();
        $this->db->exec($query);
        $this->db->commit();
        $added = true;
      }
      catch (PDOException $ex) {
        $this->db->rollback();
      }

      return $added;
    }

    // Update the old and broken feed path with the new and correct one
    public function updateFeedPath($base_url, $wrong_path, $feed_path) {
      $updated = false;

      $base_url = $this->db->quote(parse_url($base_url, PHP_URL_HOST));
      $wrong_path = $this->db->quote($wrong_path);
      $feed_path = $this->db->quote($feed_path);

      $query = "
        UPDATE websites
        SET feed_path = $feed_path
        WHERE base_url = $base_url AND feed_path = $wrong_path;
      ";

      try {
        $this->db->beginTransaction();
        $updated = $this->db->exec($query);
        $this->db->commit();
        $updated = true;
      }
      catch (PDOException $ex) {
        $this->db->rollback();
      }

      return $updated;
    }

    // Check if two "resources" reside on the same host
    public function areOnSameHost($base_url, $feed_path) {
      $base_host = parse_url($base_url, PHP_URL_HOST);
      $feed_host = parse_url($feed_path, PHP_URL_HOST);

      return intval($base_host == $feed_host);
    }

    // Update the same_host column of a specific record
    public function updateSameHost($base_url, $wrong_path, $same_host) {
      $updated = false;

      $base_url = $this->db->quote(parse_url($base_url, PHP_URL_HOST));
      $wrong_path = $this->db->quote($wrong_path);

      $query = "
        UPDATE websites
        SET same_host = $same_host
        WHERE base_url = $base_url AND feed_path = $wrong_path;
      ";

      try {
        $this->db->beginTransaction();
        $this->db->exec($query);
        $this->db->commit();
        $updated = true;
      }
      catch (PDOException $ex) {
        $this->db->rollback();
      }

      return $updated;
    }

    // Check if two "resources" reside on the same host (but query che database)
    public function getSameHost($base_url) {
      $base_url = $this->db->quote(parse_url($base_url, PHP_URL_HOST));

      $query = "
        SELECT same_host
        FROM websites
        WHERE base_url = $base_url;
      ";

      $rows = $this->db->query($query);

      if ($rows && $rows->rowCount() > 0) {
        return boolval($rows->fetch(PDO::FETCH_ASSOC)["same_host"]);
      }
      else {
        return false;
      }
    }

    // Get the specified user ID
    private function getUserId($username) {
      $username = $this->db->quote($username);

      $query = "
        SELECT id
        FROM users
        WHERE username = $username;
      ";

      $rows = $this->db->query($query);

      if ($rows && $rows->rowCount() == 1) {
        return intval($rows->fetch(PDO::FETCH_ASSOC)["id"]);
      }
      else {
        return NULL;
      }
    }

    // Delete a channel from the specified user's list
    public function deleteUserFeed($username, $website, $feed_path) {
      $deleted = false;
      $user_id = $this->getUserId($username);
      $website = $this->db->quote($website);
      $feed_path = $this->db->quote($feed_path);

      if ($user_id != NULL) {
        $query = "
          DELETE FROM following
          WHERE user = $user_id AND website = $website AND feed_path = $feed_path;
        ";

        try {
          $this->db->beginTransaction();
          $this->db->exec($query);
          $this->db->commit();
          $deleted = true;
        }
        catch (PDOException $ex) {
          $this->db->rollback();
          $deleted = false;
        }
      }

      return $deleted;
    }

    // Get all the channels of the specified users
    public function getUserFeeds($username) {
      $websites = array();
      $user_id = $this->getUserId($username);

      if ($user_id != NULL) {
        $query = "
          SELECT w.scheme, a.website, a.feed_path
          FROM following AS a JOIN websites AS w ON a.website = w.base_url AND a.feed_path = w.feed_path
          WHERE a.user = $user_id;
        ";

        $rows = $this->db->query($query);

        if ($rows && $rows->rowCount() > 0) {
          foreach ($rows as $row) {
            if (parse_url($row["feed_path"], PHP_URL_SCHEME) != "") {
              array_push($websites, $row["scheme"] . $row["website"] . " (" . $row["feed_path"] . ")");
            }
            else {
              array_push($websites, $row["scheme"] . $row["website"] . $row["feed_path"]);
            }
          }
        }
      }

      return $websites;
    }

    // Get user's related websites
    public function getWebsitesByUser($username) {
      $websites = array();
      $user_id = $this->getUserId($username);

      if ($user_id != NULL) {
        $query = "
          SELECT w.scheme, a.website
          FROM following AS a JOIN websites AS w ON a.website = w.base_url AND a.feed_path = w.feed_path
          WHERE a.user = $user_id;
        ";

        $rows = $this->db->query($query);

        if ($rows && $rows->rowCount() > 0) {
          foreach ($rows as $row) {
            array_push($websites, $row["scheme"] . $row["website"]);
          }
        }
      }

      return $websites;
    }

    // Get the specified website related to the specified user
    public function websiteRelatedToUser($username, $website, $feed_path) {
      // feed_path will never be null, because this function is always called after making the URL of the associated feed
      $user_id = $this->getUserId($username);
      $website = $this->db->quote($this->trimURLPrefix($website));

      $feed_path = $this->db->quote($feed_path);
      $query = "
        SELECT website
        FROM following
        WHERE user = $user_id AND website = $website AND feed_path = $feed_path;
      ";

      $rows = $this->db->query($query);

      return ($rows && $rows->rowCount() == 1);
    }

    // Add a channel to the specified user's list
    public function addWebsiteAssociation($username, $base_url, $feed_path) {
      $added = false;

      $user_id = $this->getUserId($username);
      $base_url = $this->db->quote($base_url);
      $feed_path = $this->db->quote($feed_path);

      $query = "
        INSERT INTO following(user, website, feed_path)
        VALUES ($user_id, $base_url, $feed_path);
      ";

      try {
        $this->db->beginTransaction();
        $this->db->exec($query);
        $this->db->commit();
        $added = true;
      }
      catch (PDOException $ex) {
        $this->db->rollback();
      }

      return $added;
    }

    // Check if the specified user exists
    public function userExists($username, $password) {
      $username = $this->db->quote($username);
      $salt = "";
      $password = $this->db->quote(hash("sha256", $salt . $password));

      $query = "
        SELECT *
        FROM users
        WHERE username = $username AND password = $password;
      ";

      $rows = $this->db->query($query);

      return ($rows && $rows->rowCount() == 1);
    }

    // Create a new user
    public function newUser($username, $password, $display_name) {
      $added = false;

      if ($this->getUserId($username) == NULL) {
        $username = $this->db->quote($username);
        $salt = "";
        $password = $this->db->quote(hash("sha256", $salt . $password));

        if ($display_name == NULL) {
          $query = "
            INSERT INTO users(username, password)
            VALUES ($username, $password);
          ";
        }
        else {
          $display_name = $this->db->quote($display_name);

          $query = "
            INSERT INTO users(username, password, display_name)
            VALUES ($username, $password, $display_name);
          ";
        }

        try {
          $this->db->beginTransaction();
          $this->db->exec($query);
          $this->db->commit();
          $added = true;
        }
        catch (PDOException $ex) {
          $this->db->rollback();
          $added = false;
        }
      }

      return $added;
    }

    // Get the display name, if set, of the specified user
    public function getDisplayName($username) {
      $display_name = "";
      $user_id = $this->getUserId($username);

      $query = "
        SELECT display_name
        FROM users
        WHERE id = $user_id;
      ";

      $rows = $this->db->query($query);

      if ($rows && $rows->rowCount() == 1) {
        $display_name = $rows->fetch(PDO::FETCH_ASSOC)["display_name"];

        if ($display_name == NULL) {
          $display_name = $username;
        }
      }

      return $display_name;
    }
  }
?>
