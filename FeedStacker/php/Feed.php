<?php
  /*
  This class represent a generic feed with all its main features
  It implements the JsonSerializable interface because the output will be a JSON object
  */
  class Feed implements JsonSerializable {
    private $icon;
    private $link;
    private $title;
    private $description;
    private $timestamp;

    public function __construct($icon, $link, $title, $description, $date) {
      // strip_tags is an input validation function
      $this->icon = strip_tags($icon);
      $this->link = strip_tags($link);
      $this->title = strip_tags($title);
      $this->description = strip_tags($description);
      $this->timestamp = strtotime(strip_tags($date));
    }

    public function getIcon() {
      return $this->icon;
    }

    public function getTitle() {
      return $this->title;
    }

    public function getLink() {
      return $this->link;
    }

    public function getDescription() {
      return $this->description;
    }

    public function getTimestamp() {
      return $this->timestamp;
    }

    // Every feed is represented as an array
    public function jsonSerialize() {
      return [
        "icon" => $this->icon,
        "title" => $this->title,
        "link" => $this->link,
        "description" => $this->description,
        "timestamp" => $this->timestamp
      ];
    }
  }
?>
