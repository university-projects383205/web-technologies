<?php
  /*
  This class provides basic and common features used across the entire project.
  */
  class Utils {
    // Connect to the specified URL
    public static function connect($url) {
      $params = array("http" => array(
          "timeout" => 3
        )
      );

      $context = stream_context_create($params);
      $page = file_get_contents($url, false, $context);

      return $page;
    }

    // Download the content (XML) from the specified URL
    public static function getXML($feed_url) {
      $xml = NULL;

      set_error_handler(function($severity, $msg, $file, $line, $context) {
        throw new ErrorException($msg, 0, $severity, $file, $line);
      }, E_WARNING);

      try {
        $page_body = Utils::connect($feed_url);
        $xml = simplexml_load_string($page_body);

        if (!$xml) {
          $xml = NULL;
        }
      }
      catch (ErrorException $ex) {
        $xml = NULL;
      }
      finally {
        restore_error_handler();

        return $xml;
      }
    }

    // Get the DOM of the specified URL
    public static function getDOM($url) {
      $dom = NULL;

      try {
        set_error_handler(function($severity, $msg, $file, $line, $context) {
          throw new ErrorException($msg, 0, $severity, $file, $line);
        }, E_WARNING);

        $dom = new DOMDocument();
        $page_body = Utils::connect($url);

        // Avoid and skip errors caused by bad HTML formatting
        libxml_use_internal_errors(true);
        $dom->loadHTML($page_body);
        libxml_clear_errors();
        libxml_use_internal_errors(false);
      }
      catch (ErrorException $ex) {
        $dom = NULL;
      }
      finally {
        restore_error_handler();
        return $dom;
      }
    }

    // Compare feeds publication date
    public static function compareFeeds($first, $second) {
      return $first->getTimestamp() < $second->getTimestamp();
    }
  }
?>
