<?php
  include("Utils.php");
  include("Feed.php");
  include("Manager.php");

  /*
  This class provides features like RSS retrieving from the specified source.
  */
  class Collector {
    private static $FEED_VALIDATOR = "https://validator.w3.org/feed/check.cgi?output=soap12&url=";
    private $feed_url;
    private $base_url;
    private $xml;
    private $manager;
    private $is_present;
    private $is_valid;

    // Split the URL in its main components: scheme, host and path
    public function __construct($url) {
      $this->manager = new Manager();
      $this->is_present = false;
      $this->is_valid = true;

      $url = urldecode($url);
      $url = trim($url, " /");

      if (parse_url($url, PHP_URL_SCHEME) == "") {
        $scheme = "http://";
        $host = parse_url($scheme . $url, PHP_URL_HOST);

        $url = $scheme . $url;
      }
      else {
        $scheme = parse_url($url, PHP_URL_SCHEME) . "://";
        $host = parse_url($url, PHP_URL_HOST);
      }

      // If a path is specified, then assume it's the complete RSS path
      if (parse_url($url, PHP_URL_PATH) == "") {
        $this->getFeedURL($url);
      }
      else {
        $this->base_url = parse_url($url, PHP_URL_SCHEME) . "://" . parse_url($url, PHP_URL_HOST);
        $this->feed_url = $url;
      }
    }

    // If the URL is not fully written, then try to find the correct one base on the specified host
    private function getFeedURL($url) {
      $url = trim($url, " /");

      if (parse_url($url, PHP_URL_SCHEME) == "") {
        $scheme = "http://";
        $host = parse_url($scheme . $url, PHP_URL_HOST);
      }
      else {
        $scheme = parse_url($url, PHP_URL_SCHEME) . "://";
        $host = parse_url($url, PHP_URL_HOST);
      }

      $url = $scheme . $host;
      $this->base_url = $url;

      if ($this->manager != NULL) {
        $this->is_present = $this->manager->websitePresent($this->base_url);
      }

      if (parse_url($url, PHP_URL_PATH) == "") {
        if ($this->is_present) {
          $url = $this->manager->getFeedLink($this->base_url);

          if ($url == NULL) {
            $url = $this->getFeedLink($this->base_url);
          }
        }
        else {
          $url = $this->getFeedLink($url);
        }
      }

      $this->feed_url = $url;
    }

    // Try to extract the RSS correct URL from the specified page source code
    private function getFeedLink($url) {
      $feeds = array();
      $dom = Utils::getDOM($this->base_url);

      if ($dom != NULL) {
        $links = $dom->getElementsByTagName("link");

        foreach ($links as $link) {
          $rel = $link->getAttribute("rel");
          $type = $link->getAttribute("type");
          $href = $link->getAttribute("href");

          if ($rel == "alternate" && $type == "application/rss+xml" && trim($href, " ") != "") {
            // The specified URL is relative
            if (parse_url($href, PHP_URL_SCHEME) == "") {
              $href = $this->base_url . $href;
            }

            array_push($feeds, $href);
          }
        }

        if (sizeof($feeds) > 0) {
          return $feeds[0];
        }
        else {
          return $url . "/rss";
        }
      }
      else {
        return $url . "/rss";
      }
    }

    // Download and analyze the RSS feed
    private function getXML() {
      $this->xml = Utils::getXML($this->feed_url);

      if ($this->xml == NULL) {
        // If the URL is broken or similar, try to "calculate" the working one
        $correct_url = $this->getFeedLink($this->base_url);

        if ($this->feed_url != $correct_url) {
          $same_host = $this->manager->areOnSameHost($this->base_url, $correct_url);

          if ($same_host) {
            $wrong_path = parse_url($this->feed_url, PHP_URL_PATH);
            $feed_path = parse_url($correct_url, PHP_URL_PATH);
            $params = parse_url($correct_url, PHP_URL_QUERY);

            if ($params != "") {
              $feed_path = $feed_path . "?$params";
            }
          }
          else {
            $wrong_path = $this->feed_url;
            $feed_path = $correct_url;
          }

          $updated = $this->manager->updateFeedPath($this->base_url, $wrong_path, $feed_path) && $this->manager->updateSameHost($this->base_url, $wrong_path, $same_host);

          if ($updated) {
            $this->xml = Utils::getXML($correct_url);
            $this->feed_url = $correct_url;

            if ($this->xml == NULL) {
              $this->is_valid = false;
            }
          }
        }
      }
    }

    // Check if the specified URL is a link to a well formatted RSS 2.0 feed
    private function isValid($feed_url) {
      $is_valid = false;

      set_error_handler(function($severity, $msg, $file, $line, $context) {
        throw new ErrorException($msg, 0, $severity, $file, $line);
      }, E_WARNING);

      try {
        $validation_result = Utils::connect(Collector::$FEED_VALIDATOR . urlencode($feed_url));

        $is_valid = strrpos($validation_result, "<m:validity>true</m:validity>");
        // TODO: check if starts with "<rss"
      }
      catch (ErrorException $ex) {
        $is_valid = false;
      }
      finally {
        restore_error_handler();

        return $is_valid;
      }

      // WARNING: This way you can disable feed validation (may cause critical errors)
      // return true;
    }

    // Analyze XML via xpath
    private function getNodes($path) {
      if ($this->xml != NULL) {
        return $this->xml->xpath($path);
      }
      else {
        return NULL;
      }
    }

    // Try to retrieve feed icon
    private function getFeedIcon() {
      $img = $this->getNodes("/rss/channel/image");

      if ($img != NULL && sizeof($img) > 0) {
        $img_url = $this->getNodes("/rss/channel/image/url")[0];
        $img_alt = $this->getNodes("/rss/channel/image/title")[0];
        $img_ref = $this->getNodes("/rss/channel/image/link")[0];

        return "<a href=\"".$img_ref."\" target=\"_blank\"><img id=\"icon\" src=\"".$img_url."\" alt=\"".$img_alt."\"/></a>";
      }
      else {
        return "<img id=\"icon\" src=\"img/rss.png\"/>";
      }
    }

    // Try various ways to get a valid icon for the specified channel
    private function getIcon() {
      $icons = array();
      $dom = Utils::getDOM($this->base_url);

      if ($dom != NULL) {
        $links = $dom->getElementsByTagName("link");

        foreach ($links as $link) {
          $rel = $link->getAttribute("rel");
          $href = $link->getAttribute("href");

          if (strpos($rel, "icon") !== false && trim($href, " ") != "") {
            if (parse_url($href, PHP_URL_SCHEME) == "") {
              $href = $this->base_url . $href;
            }

            array_push($icons, $href);
          }
        }

        if (sizeof($icons) > 0) {
          return $icons[0];
        }
        else {
          $this->getFeedIcon();
        }
      }
      else {
        $this->getFeedIcon();
      }
    }

    // Get the XML representing the feed
    public function getFeed() {
      $this->getXML();
      $feed_path = parse_url($this->feed_url, PHP_URL_PATH);
      $params = parse_url($this->feed_url, PHP_URL_QUERY);

      if ($params != "") {
        $feed_path = $feed_path . "?$params";
      }

      if ($this->xml != NULL) {
        if ($this->manager->websitePresent($this->base_url, $feed_path)) {
          // echo "$this->base_url PRESENT!<br/>";
          return $this->xml;
        }
        else {
          if ($this->isValid($this->feed_url)) {
            // echo "$this->base_url NOT present!<br/>";
            return $this->xml;
          }
          else {
            return NULL;
          }
        }
      }
      else {
        return NULL;
      }
    }

    // Find and collect all the feeds bound to the specified channel
    public function getWebsiteNews() {
      $news = array();
      $r = $this->getFeed();
      $feed_path = parse_url($this->feed_url, PHP_URL_PATH);
      $params = parse_url($this->feed_url, PHP_URL_QUERY);

      if ($params != "") {
        $feed_path = $feed_path . "?$params";
      }

      if ($r != NULL) {
        $icon = $this->manager->websitePresent($this->base_url, $feed_path) ? $this->manager->getWebsiteIcon($this->base_url) : $this->getIcon();
        $n_items = $this->getNodes("/rss/channel/item");

        // Set a default icon
        if ($icon == NULL) {
          $icon = "img/rss.png";
        }

        if ($n_items != NULL) {
          for ($i = 0; $i < sizeof($n_items); $i++) {
            $link = $n_items[$i]->link;
            $title = $n_items[$i]->title == "" ? "Title not available" : $n_items[$i]->title;
            $description = $n_items[$i]->description == "" ? "Description not available" : $n_items[$i]->description;
            $date = $n_items[$i]->pubDate == "" ? "January 1st, 1970" : $n_items[$i]->pubDate;

            array_push($news, new Feed($icon, $link, $title, $description, $date));
          }
        }
      }

      return $news;
    }

    // The specified user add a channel to his list
    public function addFeed($username) {
      $r = $this->getFeed();

      if ($r != NULL) {
        $base_host = $this->manager->trimURLPrefix($this->base_url);
        $feed_path = parse_url($this->feed_url, PHP_URL_PATH);
        $params = parse_url($this->feed_url, PHP_URL_QUERY);

        if ($params != "") {
          $feed_path = $feed_path . "?$params";
        }

        if (!$this->manager->websitePresent($this->base_url, $feed_path)) {
          // In questo modo (riga commentata) salva come base_url quello ricavato dalla lettura del tag alternate contenuto nella pagina specificata dall'utente, invece di impostargli come valore quello scritto dall'utente
          // $base_url = parse_url($this->feed_url, PHP_URL_SCHEME) . "://" . parse_url($this->feed_url, PHP_URL_HOST);

          $scheme = parse_url($this->base_url, PHP_URL_SCHEME) . "://";
          $icon = $this->getIcon($this->base_url);
          $feed_host = $this->manager->trimURLPrefix($this->feed_url);

          if ($base_host == $feed_host) {
            $feed_path = parse_url($this->feed_url, PHP_URL_PATH);
            $params = parse_url($this->feed_url, PHP_URL_QUERY);

            if ($params != "") {
              $feed_path = $feed_path . "?$params";
            }

            $same_host = true;
          }
          else {
            $feed_path = $this->feed_url;
            $same_host = false;
          }

          $this->manager->addWebsite($scheme, $base_host, $feed_path, $same_host, $icon);
        }

        if (!$this->manager->websiteRelatedToUser($username, $base_host, $feed_path)) {
          $this->manager->addWebsiteAssociation($username, $base_host, $feed_path);
        }

        return true;
      }
      else {
        return false;
      }
    }
  }
?>
