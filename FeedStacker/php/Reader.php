<?php
  include("Collector.php");

  class Reader {
    private $manager;
    private $username;

    public function __construct($username) {
      $this->manager = new Manager();
      $this->username = $username;
    }

    // Get all user followed channels and retrieve their related feeds
    public function getNews() {
      $all_feeds = array();
      $websites = $this->manager->getWebsitesByUser($this->username);

      foreach ($websites as $website) {
        $collector = new Collector($website);
        array_push($all_feeds, $collector->getWebsiteNews());
      }

      // Sort feeds by publication date, in descending order
      $all_feeds = call_user_func_array("array_merge", $all_feeds);
      usort($all_feeds, "Utils::compareFeeds");

      return $all_feeds;
    }
  }
?>
