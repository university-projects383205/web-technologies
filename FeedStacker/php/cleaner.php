<?php
  // Retrieve user feeds and encode them as JSON objects
  function getUserFeeds($username) {
    $manager = new Manager();

    $user_feeds = $manager->getUserFeeds($username);

    echo json_encode($user_feeds, JSON_PRETTY_PRINT | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
  }

  // Remove a feed from the list of the specified user
  function deleteUserFeed($username, $to_delete) {
    $manager = new Manager();
    $to_delete = urldecode($to_delete);

    if (strpos($to_delete, " (http") === false) {
      $base_host = parse_url($to_delete, PHP_URL_HOST);
      $feed_path = parse_url($to_delete, PHP_URL_PATH);
      $params = parse_url($to_delete, PHP_URL_QUERY);

      if ($params != "") {
        $feed_path = $feed_path . "?$params";
      }
    }
    else {
      $website = substr($to_delete, 0, strpos($to_delete, " ("));

      $base_host = parse_url($website, PHP_URL_HOST);
      $feed_path = substr($to_delete, strlen($website) + 2, strlen($to_delete) - (strlen($website) + 2) - 1);
    }

    $to_delete = $manager->deleteUserFeed($username, $base_host, $feed_path);
  }
?>
