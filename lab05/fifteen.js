/* GLOBAL VARIABLES
*  game_started     ->  Game status
*  start_timestamp  ->  Game start timestamp
*/
var game_started = false;
var start_timestamp;

// Functions to run on window load
$(document)
  .ready(
    function() {
      const MAX_ITERATIONS = 28;
      var correct_coords = getCoords(); // Get correct coordinations

      setupPuzzle();
      enableListeners(correct_coords);

      $("#shufflebutton")
        .click(
          function() {
            shuffle(MAX_ITERATIONS);
            $("#congrats").remove();
            window.game_started = true;
            window.start_timestamp = event.timeStamp;
          }
        );
    }
  );

// Setup tiles background-position
function setupPuzzle() {
  var tiles = $("#puzzlearea > div");
  var x_pos = 0;
  var y_pos = 0;

  for (var i = 0; i < tiles.length; i++) {
    $(tiles[i]).css("background-position", x_pos + "px " + y_pos + "px");
    x_pos -= 100;

    if (x_pos === -400) {
      x_pos = 0;
      y_pos -= 100;
    }
  }

  $("#puzzlearea").css("visibility", "visible");
}

// Enable tiles listeners
function enableListeners(correct_coords) {
  $("#puzzlearea > div")
    .click(
      function() {
        moveTile(this);
        if (window.game_started) {
          checkWin(correct_coords);
        }
      }
    );

  $("#puzzlearea > div")
    .mouseover(
      function() {
        var current_tile = {
          x: $(this).offset().left,
          y: $(this).offset().top
        };
        var empty_tile = getEmptyTile();
        var r = areClose(current_tile, empty_tile);

        if (r.top || r.bottom || r.left || r.right) {
          $(this).css({
            "border-color": "red",
            "color": "red"
          });
        }
      }
    );

    $("#puzzlearea > div")
      .mouseout(
        function() {
          $(this).css({
            "border-color": "black",
            "color": "black"
          });
        }
      );
}

// Get tiles coordinations
function getCoords() {
  var tiles = $("#puzzlearea > div");
  var positions = new Array();

  for (var i = 0; i < tiles.length; i++) {
    var coords = {
      x: $(tiles[i]).offset().left,
      y: $(tiles[i]).offset().top
    };

    positions.push(coords);
  }

  return positions;
}

// Retrieve empty tile coordinations
function getEmptyTile() {
  var positions = getCoords();
  var x_occ = new Map();
  var y_occ = new Map();

  for (var i = 0; i < positions.length; i++) {
    if (x_occ.has(positions[i].x)) {
      x_occ.set(positions[i].x, x_occ.get(positions[i].x) + 1);
    }
    else {
      x_occ.set(positions[i].x, 1);
    }

    if (y_occ.has(positions[i].y)) {
      y_occ.set(positions[i].y, y_occ.get(positions[i].y) + 1);
    }
    else {
      y_occ.set(positions[i].y, 1);
    }
  }

  var min = Number.MAX_VALUE;
  var tile_x;
  var tile_y;
  var tile;

  for (var key of x_occ.keys()) {
    if (x_occ.get(key) <= min) {
      min = x_occ.get(key);
      tile_x = key;
    }
  }

  min = Number.MAX_VALUE;

  for (var key of y_occ.keys()) {
    if (y_occ.get(key) <= min) {
      min = y_occ.get(key);
      tile_y = key;
    }
  }

  tile = {
    x: tile_x,
    y: tile_y
  };

  return tile;
}

// Check if two tiles are close to each other
function areClose(tile1, tile2) {
  var check_top = tile1.y - 100 === tile2.y && tile1.x === tile2.x;
  var check_bottom = tile1.y + 100 === tile2.y && tile1.x === tile2.x;
  var check_left = tile1.x - 100 == tile2.x && tile1.y === tile2.y;
  var check_right = tile1.x + 100 === tile2.x && tile1.y === tile2.y;

  var result = {
    top: check_top,
    bottom: check_bottom,
    left: check_left,
    right: check_right,
  };

  return result;
}

// Move the specified tile
function moveTile(tile) {
  var current_tile = {
    x: $(tile).offset().left,
    y: $(tile).offset().top
  };
  var empty_tile = getEmptyTile();
  var r = areClose(current_tile, empty_tile);

  if (r.top) {
    $(tile).offset({
      top: current_tile.y - 100
    });
  }
  else if (r.bottom) {
    $(tile).offset({
      top: current_tile.y + 100
    });
  }
  else if (r.left) {
    $(tile).offset({
      left: current_tile.x - 100
    });
  }
  else if (r.right) {
    $(tile).offset({
      left: current_tile.x + 100
    });
  }

  if ((!window.game_started) && (r.top || r.bottom || r.left || r.right)) {
    $("#congrats").remove();
    window.game_started = true;
    window.start_timestamp = event.timeStamp;
  }
}

// Randomly move a tile
function moveRandomTile(prev_tile) {
  var tiles = $("#puzzlearea > div");
  var empty_tile = getEmptyTile();
  var available_tiles = new Array();

  for (var i = 0; i < tiles.length; i++) {
    var current_tile = {
      x: $(tiles[i]).offset().left,
      y: $(tiles[i]).offset().top
    };
    var r = areClose(current_tile, empty_tile);

    if (r.top || r.bottom || r.left || r.right) {
      available_tiles.push(tiles[i]);
    }
  }

  var random_tile = available_tiles[Math.floor(Math.random() * available_tiles.length)];

  while ($(random_tile).html() === $(prev_tile).html()) {
    random_tile = available_tiles[Math.floor(Math.random() * available_tiles.length)];
  }

  moveTile(random_tile);

  return random_tile;
}

// Shuffle tiles
function shuffle(max_iterations) {
  const ITERATIONS_N = max_iterations > 0 ? Math.floor(Math.random() * max_iterations + 1) : 1;
  var prev_tile;

  for (var i = 0; i < ITERATIONS_N; i++) {
    var curr_tile = moveRandomTile(prev_tile);
    prev_tile = curr_tile;
  }
}

// Check if player won
function checkWin(correct_coords) {
  var is_correct = true;
  var current_coords = getCoords();

  for (var i = 0; i < correct_coords.length && is_correct; i++) {
    if (correct_coords[i].x !== current_coords[i].x || correct_coords[i].y !== current_coords[i].y) {
      is_correct = false;
    }
  }

  if (is_correct) {
    const end_timestamp = event.timeStamp;
    var total_seconds = (end_timestamp - window.start_timestamp) / 1000;
    const spent_time = total_seconds >= 1 ? getSpentTime(total_seconds) : Math.floor(total_seconds) + " seconds";

    $("#puzzlearea").after("<div id=\"congrats\"><br/><strong>YOU WON!</strong><br/><strong>(" + spent_time + ")</strong><br/></div>");
    window.game_started = false;
  }
}

// Calculate time spent until victory
function getSpentTime(seconds) {
  var minutes = 0;
  var hours = 0;

  while (seconds >= 60) {
    minutes += 1;
    seconds -= 60;
  }

  while (minutes >= 60) {
    hours += 1;
    minutes -= 60;
  }

  seconds = Math.floor(seconds);

  var seconds_title = seconds !== 1 ? " seconds" : " second";
  var minutes_title = minutes !== 1 ? " minutes " : " minute ";
  var hours_title = hours !== 1 ? " hours " : " hour ";

  if (hours === 0 && minutes === 0) {
    return seconds + seconds_title;
  }
  else if (hours === 0) {
    return minutes + minutes_title + seconds + seconds_title;
  }
  else {
    return hours + hours_title + minutes + minutes_title + seconds + seconds_title;
  }
}
