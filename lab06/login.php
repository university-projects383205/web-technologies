<?php
  session_start();
  include("common.php");

  // If the user is logged-in, destroy his session
  if (isset($_SESSION["id"])) {
    session_unset();
    session_destroy();
    ?>
    <div class="info">Logout successful!</div>
    <?php
  }

  // If the user is not logged-in, show an error message
  if (isset($_SESSION["msg"])) {
    ?>
    <div class="error"><?= $_SESSION["msg"] ?></div>
    <?php
    unset($_SESSION["msg"]);
  }
  else {
    // Check if the specified user exists
    if (isset($_POST["username"]) && isset($_POST["password"])) {
      /*
      *  USERS TABLE
      *
      *  +----+----------+-------------+
      *  | id | username | password    |
      *  +----+----------+-------------+
      *  |  1 | alex     | qwerty      |
      *  |  2 | tom      | password123 |
      *  |  3 | frank    | golfclub    |
      *  +----+----------+-------------+
      *
      *  P.S. These passwords are shown in plain text but they're supposed to be hashed with SHA256.
      */

      $username = $_POST["username"];
      $password = $_POST["password"];

      $db = connect();

      $un = $db->quote($username);
      $pw = $db->quote(hash("sha256", $password));

      $query = "
        SELECT id
        FROM users
        WHERE username = $un AND password = $pw;
        ";
      $rows = $db->query($query);

      if ($rows && $rows->rowCount() > 0) {
        $_SESSION["id"] = $rows->fetch(PDO::FETCH_ASSOC)["id"];
        $_SESSION["username"] = $username;
        $_SESSION["password"] = $password;

        header("Location: index.php");
      }
      else {
        ?>
        <div class="error">Invalid username and/or password!</div>
        <?php
      }
    }
  }
?>
        <form action="<?php $_SERVER["PHP_SELF"] ?>" method="post">
          <div>
            <label>Username:</label>
            <input id="username" name="username" type="text" maxlength="28"/>
          </div>
          <br/>
          <div>
            <label>Password:</label>
            <input id="password" name="password" type="password"/>
          </div>
          <br/>
          <input type="submit" value="Login"/>
        </form>
      </div> <!-- end of #main div -->

      <div id="w3c">
      <a href="http://validator.w3.org/check/referer">
        <img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/w3c-html.png" alt="Valid HTML" >
      </a>
      <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS" >
      </a>
      </div>
    </div> <!-- end of #frame div -->
  </body>
</html>
