<?php
  session_start();

  if (isset($_SESSION["id"])) {
    include("common.php");

    $firstname = $_GET["firstname"];
    $lastname = $_GET["lastname"];

    $db = connect();

    if ($db) {
      $id = getActor($firstname, $lastname);

      // Select movies in which selected actor, and Kevin Bacon, are present
      $query = "
        SELECT m.name, m.year
        FROM movies AS m JOIN roles AS r ON m.id = r.movie_id JOIN actors AS a ON r.actor_id = a.id
        WHERE a.id = $id AND m.id IN
          (
            SELECT m2.id
            FROM movies as m2 JOIN roles AS r2 ON m2.id = r2.movie_id JOIN actors AS a2 ON a2.id = r2.actor_id
            WHERE a2.first_name = \"Kevin\" AND a2.last_name = \"Bacon\"
          )
        ORDER BY m.year DESC, m.name ASC;
      ";

      $rows = $db->query($query);

      if (!$rows || $rows->rowCount() == 0) {
        $query = "
          SELECT COUNT(*) AS tot
          FROM actors
          WHERE id = $id;
        ";

        $rows = $db->query($query);

        if ($rows && $rows->rowCount() > 0) {
          $tot = $rows->fetch(PDO::FETCH_ASSOC)["tot"];

          if ($tot > 0) {
            ?>
            <h1><?= $firstname ?> <?= $lastname ?> wasn't in any films with Kevin Bacon.</h1>
            <?php
          }
        }
        else {
          ?>
          <h1>Actor <?= $firstname ?> <?= $lastname ?> not found.</h1>
          <?php
        }
      }
      else {
        drawTable($rows, $firstname, $lastname);
      }
    }
    else {
      ?>
      <h1 class="errmsg">Unexpected error!</h1>
      <?php
    }

    include("bottom.html");
  }
  else {
    $_SESSION["msg"] = "You must login first!";
    header("Location: login.php");
  }
?>
