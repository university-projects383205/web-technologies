<?php
  session_start();
  include("top.php");
?>

<h1>The One Degree of Kevin Bacon</h1>
<p>Type in an actor's name to see if he/she was ever in a movie with Kevin Bacon!</p>
<p><img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/5/kevin_bacon.jpg" alt="Kevin Bacon" ></p>

<?php
  // If user is not logged-in, redirect the client to login page
  if (isset($_SESSION["id"])) {
    include("bottom.html");
  }
  else {
    $_SESSION["msg"] = "You must login first!";
    header("Location: login.php");
  }
?>
