<?php
  session_start();

  if (isset($_SESSION["id"])) {
    include("common.php");

    $firstname = $_GET["firstname"];
    $lastname = $_GET["lastname"];

    $db = connect();

    if ($db) {
      $id = getActor($firstname, $lastname);

      // Select movies in which the selected actor is present
      $query = "
        SELECT m.name, m.year
        FROM movies AS m join roles AS r ON m.id = r.movie_id JOIN actors AS a ON r.actor_id = a.id
        WHERE a.id = $id
        ORDER BY m.year DESC, m.name ASC;
      ";

      $rows = $db->query($query);

      if ($rows && $rows->rowCount() > 0) {
        drawTable($rows, $firstname, $lastname);
      }
      else {
        ?>
        <h1>Actor <?= $firstname ?> <?= $lastname ?> not found.</h1>
        <?php
      }
    }
    else {
      ?>
      <h1 class="errmsg">Unexpected error!</h1>
      <?php
    }

    include("bottom.html");
  }
  else {
    $_SESSION["msg"] = "You must login first!";
    header("Location: login.php");
  }
?>
