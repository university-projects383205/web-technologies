<?php
  include("top.php");

  // Connect to IMDB server managed by MySQL DBMS via a PDO (PHP Data Object)
  function connect() {
    $db = NULL;

    try {
      $db = new PDO("mysql:host=localhost;dbname=IMDB", "root", "admin");
    }
    catch (PDOException $ex) {
      ?>
      <h1 class="errmsg">Exception occured!</h1>
      <p><?= $ex ?></p>
      <?php
    }

    return $db;
  }

  // Get actor ID based on actor's first name and last name
  function getActor($firstname, $lastname) {
    $db = connect();

    if ($db) {
      $fn = $db->quote($firstname."%");
      $ln = $db->quote($lastname);

      $query = "
        SELECT id
        FROM actors
        WHERE first_name LIKE $fn AND last_name = $ln
        ORDER BY film_count DESC, id ASC
        LIMIT 1;
      ";
      $rows = $db->query($query);

      return $rows->fetch(PDO::FETCH_ASSOC)["id"];
    }
    else {
      ?>
      <h1 class="errmsg">Unexpected error!</h1>
      <?php

      return NULL;
    }
  }

  // Create and populate a table based on a query results
  function drawTable($rows, $firstname, $lastname) {
    if ($rows && $rows->rowCount() > 0) {
      ?>
      <h1>Results for <?= $firstname ?> <?= $lastname ?></h1>
      <p id="capt">All Films</p>
      <table>
        <tr><th>#</th><th>Title</th><th>Year</th></tr>
        <?php
          $counter = 1;
          foreach ($rows as $row) {
        ?>
            <tr><td><?= $counter ?></td><td><?= $row["name"] ?></td><td><?= $row["year"] ?></td></tr>
        <?php
            $counter++;
          }
        ?>
      </table>
      <?php
    }
  }
?>
