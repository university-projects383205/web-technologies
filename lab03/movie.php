<!--
  Nome:             Daniele
  Cognome:          Liberatore
  Anno accademico:  2019/2020
  Corso:            B
  Descrizione:      Pagina dinamica per la visualizzazione di informazioni relative a differenti film mostrati con lo stesso layout
 -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link href="css/movie.css" type="text/css" rel="stylesheet"/>
    <link href="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rotten.gif" type="image/gif" rel="shortcut icon">
    <title>Rancid Tomatoes</title>
  </head>
  <body>
    <?php
      // Disabilito la funzionalità di error reporting
      error_reporting(0);

      // Ottengo il nome del film e lo uso come path per leggere i file di cui necessito
      $movie = $_GET["film"];
      list($title, $date, $vote) = file($movie."/info.txt");

      // Funzione per far sì che il valore di $val sia, al più, uguale a quello di $limit
      function constrain($val, $limit) {
        return $val > $limit ? $limit : $val;
      }
    ?>
    <div id="banner">
      <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes"/>
    </div>

    <!-- Imposto il titolo e la data di uscita del film -->
    <h1 id="title"><?= $title."(".trim($date).")" ?></h1>
    <div id="main">
      <div id="rightbox">
        <div id="poster">
          <img src="<?= $movie."/overview.png" ?>" alt="General overview"/>
        </div>

        <div id="infos">
          <dl>
            <?php
              /* Ogni linea di overview.txt va spezzata in due parti in presenza del carattere ':'
              *   => la prima rappresenta il contenuto del tag <dt>
              *   => la seconda rappresenta il contenuto del tag <dd>
              */
              $lines = file($movie."/overview.txt");
              foreach ($lines as $line) {
                $dt = trim(explode(":", $line)[0]);
                $dd = trim(explode(":", $line)[1]);
            ?>
            <dt><?= $dt ?></dt>
            <dd><?= $dd ?></dd>
            <?php
              }
            ?>
          </dl>
        </div>
      </div>

      <div id="leftbox">
        <div id="vote">
          <?php
            // Imposto l'URL per ottenere le risorse remote e imposto $status (l'icona rappresentante la valutazione del film) in base al voto ottenuto
            $url = "https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/";
            $vote >= 60 ? $status = "fresh" : $status = "rotten";
          ?>
          <!-- Imposto l'immagine relativa al voto ottenuto (con annesso attributo "alt") -->
          <!-- L'attributo "alt" è composto in questo modo per far sì che solo la prima lettera sia maiuscola -->
          <img src="<?= $url.$status."big.png" ?>" alt="<?= strtoupper(substr($status, 0, 1)).substr($status, 1) ?>"/>
          <span id="percent"><?= $vote."%" ?></span>
        </div>

        <div id="reviews">
          <?php
            /* Ogni file "review*.txt" contiene una recensione
             * Eseguo una selezione tramite pattern matching (seleziono tutti i file che iniziano per review, sono seguiti da un numero indefinito di caratteri e terminano con .txt)
            */
            $reviews = glob($movie."/review*.txt");
            $reviews_n = sizeof($reviews);
            $first_n = constrain(round($reviews_n / 2), 5);
          ?>
          <div id="first">
            <?php
              /* Funzione per il popolamento della colonna dei commenti, dove:
              *   $reviews  =>  indica il vettore contentente le recensioni
              *   $url      =>  indica l'URL per ottenere le risorse remote
              *   $start    =>  indica l'indice, del vettore $reviews, da cui partire per il popolamento
              *   $end      =>  indica l'indice finale, del vettore $reviews, a cui giungere per il popolamento
              */
              function reviews($reviews, $url, $start, $end) {
                for ($i = $start; $i < $end; $i++) {
                  list($comment, $icon, $author, $publication) = file($reviews[$i]);
            ?>
            <p class="comment">
              <img src="<?= trim($url.strtolower($icon)).".gif" ?>" alt="<?= trim(substr($icon, 0, 1).strtolower(substr($icon, 1))) ?>"/>
              <q><?= $comment ?></q>
            </p>
            <p class="author">
              <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic"/>
              <?= $author ?><br/>
              <span class="publication"><?= $publication ?></span>
            </p>
            <?php
                }
              }

              // Rappresento la colonna di sinistra dei commenti
              reviews($reviews, $url, 0, $first_n);
            ?>
          </div>

          <div id="second">
            <?php
              // Rappresento la colonna di destra dei commenti
              $end = constrain($reviews_n, 10);
              reviews($reviews, $url, $first_n, $end);
            ?>
          </div>
        </div>
      </div>

      <!-- Imposto il contatore di pagina -->
      <div id="counter">
        <p>(1-<?= $end ?>) of <?= $reviews_n ?></p>
      </div>
    </div>

    <div id="validators">
      <a href="https://validator.w3.org/#validate_by_upload" target="_blank">
        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/w3c-xhtml.png" alt="Validate HTML"/>
      </a>
      <br/>
			<a href="https://jigsaw.w3.org/css-validator/#validate_by_upload" target="_blank">
        <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"/>
      </a>
    </div>
  </body>
</html>
