A taut, darkly comic drama about the dilemmas of international intervention in civil war, all of it neatly symbolized by one elusive length of rope. It is also, sadly, a film much marred by its sexism.
ROTTEN
Kate Taylor
Globe and Mail
