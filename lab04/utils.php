<?php
  // Get user related data
  function get_user($path, $id) {
    $info = array("name" => $id);
    $lines = file($path);
    $found = false;

    for ($i = 0; $i < sizeof($lines) && !$found; $i++) {
      list($name, $gender, $age, $personality, $os, $seeking_min, $seeking_max, $seeking_gender) = explode(",", $lines[$i]);

      if ($name == $id) {
        $info["gender"] = $gender;
        $info["age"] = $age;
        $info["personality"] = $personality;
        $info["os"] = $os;
        $info["seeking_min"] = $seeking_min;
        $info["seeking_max"] = $seeking_max;
        $info["seeking_gender"] = $seeking_gender;

        $found = true;
      }
    }

    return $info;
  }

  // Check genders compatibility
  function check_gender($str1, $str2) {
    if ($str2 == "B") {
      return true;
    }
    else {
      return $str1 == $str2;
    }
  }

  // Check personality compatibility
  function check_type($str1, $str2) {
    $found = false;
    $shortest = min(strlen($str1), strlen($str2));

    for ($i = 0; !$found && $i < $shortest; $i++) {
      $found = $str1[$i] == $str2[$i];
    }

    return $found;
  }
?>
