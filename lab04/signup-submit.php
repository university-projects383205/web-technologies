<?php include("top.html"); ?>

<?php
  error_reporting(-1);

  // Read posted parameters
  $name = $_POST["name"];
  $gender = $_POST["gender"];
  $age = $_POST["age"];
  $personality = $_POST["personality"];
  $os = $_POST["os"];
  $seeking_min = $_POST["seeking_min"];
  $seeking_max = $_POST["seeking_max"];
  $seeking_gender = $_POST["seeking_gender"];

  // Update singles.txt
  $singles = "singles.txt";
  $record = $name.",".$gender.",".$age.",".$personality.",".$os.",".$seeking_min.",".$seeking_max.",".$seeking_gender.PHP_EOL;
  file_put_contents($singles, $record, FILE_APPEND | LOCK_EX);
?>

<p>
  <strong>Thank you!</strong>
</p>
<p>
  Welcome to NerdLuv, <?= $name ?>!
</p>
<p>
  Now <a href="matches.php" target="_blank">log in to see your matches!</a>
</p>


<?php include("bottom.html"); ?>
