<?php include("top.html"); ?>

<!-- Sign-up form -->
<form action="signup-submit.php" method="post">
  <fieldset>
    <legend>New User Signup:</legend>
    <label><strong>Name:</strong></label>
    <input type="text" name="name" size="16"/>
    <br/>
    <label><strong>Gender:</strong></label>
    <label>
      <input type="radio" name="gender" value="M"/>Male
    </label>
    <label>
      <input type="radio" name="gender" value="F" checked/>Female
    </label>
    <br/>
    <label><strong>Age:</strong></label>
    <input type="text" name="age" size="6" maxlength="2"/>
    <br/>
    <label><strong>Personality type:</strong></label>
    <input type="text" name="personality" size="6" maxlength="4"/>
    <label>
      (<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp">Don't know your type?</a>)
    </label>
    <br/>
    <label><strong>Favorite OS:</strong></label>
    <select name="os">
      <option value="Windows">Windows</option>
      <option value="Mac OS X">Mac OS X</option>
      <option value="Linux" selected>Linux</option>
    </select>
    <br/>
    <label><strong>Seeking age:</strong></label>
    <input type="text" name="seeking_min" size="6" maxlength="2" placeholder="min"/>
    <label>to</label>
    <input type="text" name="seeking_max" size="6" maxlength="2" placeholder="max"/>
    <br/>
    <label><strong>Seeking gender:</strong></label>
    <label>
      <input type="radio" name="seeking_gender" value="M"/>Male
    </label>
    <label>
      <input type="radio" name="seeking_gender" value="F"/>Female
    </label>
    <label>
      <input type="radio" name="seeking_gender" value="B" checked/>Both
    </label>
    <br/>
    <input type="submit" value="Sign Up"/>
  </fieldset>
</form>

<?php include("bottom.html"); ?>
