<?php
  include("top.html");
  include("utils.php");

  error_reporting(-1);

  $profile = $_GET["name"];
  $singles = "singles.txt";
  $user = get_user($singles, $profile);
  $ideals = array();

  $lines = file($singles);
  foreach ($lines as $line) {
    $info = explode(",", $line);
    list($name, $gender, $age, $personality, $os, $seeking_min, $seeking_max, $seeking_gender) = $info;

    if (trim($name) != trim($user["name"])) {
      if (
        check_gender(trim($gender), trim($user["seeking_gender"])) &&
        trim($age) >= trim($user["seeking_min"]) &&
        trim($age) <= trim($user["seeking_max"]) &&
        trim($os) == trim($user["os"]) &&
        check_type(trim($personality), trim($user["personality"]))
      ) {
        array_push($ideals, $info);
      }
    }
  }
?>

<h1>Matches for <?= $profile ?></h1>
  <?php
    foreach ($ideals as $elem) {
      list($name, $gender, $age, $personality, $os, $seeking_min, $seeking_max, $seeking_gender) = $elem;
  ?>
  <div class="match">
      <p>
        <img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/user.jpg" alt="User"/>
        <?= $name ?>
      </p>
      <ul>
        <li>
          <strong>gender:</strong><?= $gender ?>
        </li>
        <li>
          <strong>age:</strong><?= $age ?>
        </li>
        <li>
          <strong>type:</strong><?= $personality ?>
        </li>
        <li>
          <strong>OS:</strong><?= $os ?>
        </li>
      </ul>
  </div>
  <?php
    }
  ?>

<?php include("bottom.html"); ?>
